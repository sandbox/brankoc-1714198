jQuery Wrapper API
==================

This module offers a PHP function that will wrap your inline Javascript 
for you in the manner described at http://drupal.org/node/171213.

You can call this function from template.php, for example, or from
your own module. 

The function jquery_wrapper_api_wrap() takes three arguments: 

1. A string containing a behavior name. This name will be sanitized to 
contain only letters and digits. 

2. A string or an array of strings containing jQuery and Javascript 
snippets. This string will not be sanitized, so be careful allowing 
user input here. 

3. (Optional) A boolean indicating whether the behavior should be 
forced or not. If the behavior exists already and this flag is set 
to TRUE, the old behavior is overwritten. If the behavior exists and 
the flag is set to FALSE (default), the function will return FALSE 
and fire an error message.  

The function will return FALSE upon error. 

The sanitiation process may result in errors. For example, if you 
call the function twice, once with a behavior name called 'myModule' 
and the second time with a behavior name called 'myModule_', both 
names will be sanitised to 'myModule', the second name will be 
sanitised to the first and possibly produce a 'Behavior already 
defined' error.

Examples
--------

1. First example.

The following PHP code: 

  <?php
    if (function_exists('jquery_wrapper_api_wrap')) {
      $jquery = '$(".title").hide();';
      print jquery_wrapper_api_wrap('myNodeTplPhp', $jquery);
    }
  ?>
  
prints the following HTML : 

  <script type="text/javascript">
    <!-- 
      (function ($) {
        Drupal.behaviors.myNodeTplPhp = {
          attach: function (context, settings) {

            $(".title").hide();
          
          }
        };
      }(jQuery));
    // -->
  </script>

2. Second example. 

For longer Javascript programs you can use the PHP Heredoc syntax: 

  <?php
    $js = <<<END
      //
      // Put your Javascript program here.
      //
END;
  ?>

This stores the lines between the first and the second 'END' in 
a PHP variable which can then be fed to the wrap function.
  
Note that Heredoc is picky about white space. The first 'END' should
be followed by a newline, the second END should not be preceded by 
white space, and only be followed by a semi-colon and newline.
